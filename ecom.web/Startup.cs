﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ecom.web.Startup))]
namespace ecom.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
