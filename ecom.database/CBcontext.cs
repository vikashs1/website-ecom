﻿using ecom.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecom.database
{
    public class CBcontext : DbContext
    {
        public CBcontext() : base("ecomconnection1")
        {

        }
        public DbSet<Product> Products { get; set; }

        public DbSet<category> Categories  { get; set; }
    }
}
