namespace ecom.database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initilazed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.categories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Descrption = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        Descrption = c.String(),
                        category_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.categories", t => t.category_id)
                .Index(t => t.category_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "category_id", "dbo.categories");
            DropIndex("dbo.Products", new[] { "category_id" });
            DropTable("dbo.Products");
            DropTable("dbo.categories");
        }
    }
}
